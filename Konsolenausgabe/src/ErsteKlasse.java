class Fahrkartenautomat
{
    public static void main(String[] args)
    {
		System.out.println("Das ist ein Beispielsatz.\n \"Ein Beispielsatz\" ist das.");

		// "Das ist ein “Beispielsatz“. Ein Beispielsatz ist das."
		int alter = 21;
		String satz = "Beispielsatz";
		String name1 = "Beispielsatz 1";
		System.out.println("Das ist ein " + name1 + ". Ein " + satz + " ist das.");
	
	//A1.5 AB Ausgabenformatirung 1.1
	
		String satz1 = "Die Schule soll endlich ";
		String satz2 = "ein \"Ende\" haben.";
		
		System.out.print("Die Schule ist doof. ");
	
	//println() setzt einen Zeilenumbruch nach dem ausgegebenen Text, print() tut dies nicht.
	
	System.out.println(satz1 + satz2);
		System.out.println("");
	
	
	
	//A1.5 AB AUsgabenformatirung 1.3 Baumm
		String B1 = "*";
		System.out.printf( "\n%11s\n", "*");					// Baumspitze 1x
		
	
		String B2 = "***";
		System.out.printf( "%12s",     "***");     		    // Baummspitze 2x
	
		String B3 = "*****";
		System.out.printf( "\n %12s\n" ,    "*****");			// Baumspitze 3x
		
		String B4 = "*******"; 
		System.out.printf(  "%14s", "*******");					// Baumspitze 4x
		
	
		String B5 = "*********"; 
		System.out.printf( "\n%15s\n", "*********");               // Baumspitze 5x
		
		String B6 = "***";
		System.out.printf(  "%12s", "***");                          //Baumspitze 6x
		
		String B7 = "***";
		System.out.printf("\n%12s\n", "***");						//Baumspitze    7x	
		
		// 5 Zahlen 
		
		String Z1 = "<trerminated> Aufgabe3[Java Application] ";
		System.out.printf("<trerminated> Aufgabe3[Java Application]");
		
		double d = 22.42;
		System.out.printf( "\n %.2f \n" , d );
		
		double p1 = 111.22;
		System.out.printf(  " %.2f " , p1);
		
		double p2 = 4;
		System.out.printf(  "\n %.2f \n", p2);
		
		double p3 =  1000000.55;
		System.out.printf( " %.2f " , p3);
		
		double p7 = 97.34;
		System.out.printf( " \n %.2f \n", p7 );
		
		
		//Zhalen reihe ende
		
		
		
		//A1.6 AB Ausgabeformatierung 2.1 ohne Leerzeichen
		
		System.out.printf("%5s\n", "**");
		
		System.out.print("*");
		
		System.out.printf("%7s\n", "*");
		
		System.out.print("*");
		
		System.out.printf("%7s\n", "*");
		
		System.out.printf("%5s\n", "**");
		
		System.out.println("");
		
		
		//A1.6 AB Ausgabeformatierung 2.2
		
		System.out.printf("%-5s = ", "!0");
		
		System.out.printf("%-19s = ", "");
		
		System.out.printf("%4s\n", "1");
		
		
		System.out.printf("%-5s = ", "!1");
		
		System.out.printf("%-19s = ", "1");
		
		System.out.printf("%4s\n", "1");
		
		
		System.out.printf("%-5s = ", "!2");
		
		System.out.printf("%-19s = ", "1 * 2");
		
		System.out.printf("%4s\n", "2");
		
		
		System.out.printf("%-5s = ", "!3");
		
		System.out.printf("%-19s = ", "1 * 2 * 3");
		
		System.out.printf("%4s\n", "6");
		
		System.out.printf("%-5s = ", "!4");
		
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4");
		
		System.out.printf("%4s\n", "24");
		
		
		System.out.printf("%-5s = ", "!5");
		
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4 * 5");
		
		System.out.printf("%4s\n", "120");
		
		System.out.println("");
		
		
		System.out.printf("%-5s = ", "!2");
		
		System.out.printf("%-19s = ", "1 * 2");
		
		System.out.printf("%4s\n", "2");
		
		
		System.out.printf("%-5s = ", "!3");
		
		System.out.printf("%-19s = ", "1 * 2 * 3");
		
		System.out.printf("%4s\n", "6");
		
		
		System.out.printf("%-5s = ", "!4");
		
		System.out.printf("%-19s = ", "1 * 2 * 3 * 4");
		
		System.out.printf("%4s\n", "24");
		
		
		System.out.printf("%-5s = ", "!5");
		
		System.out.printf("%4s\n", "120");
		System.out.println("");
		
		
		
		//A1.6 AB Ausgabeformatierung 2.3
		
		
		System.out.printf("%-12s|", "Fahrenheit");
		
		System.out.printf("%10s\n", "Celsius");
		
		System.out.println("-----------------------");
		
		
		System.out.printf("%-12s|", "-20");
		
		System.out.printf("%10.2f\n", -28.8889);
		
		
		System.out.printf("%-12s|", "-10");
		
		System.out.printf("%10.2f\n", -23.3333);
		
		
		System.out.printf("%-12s|", "+0");
		
		System.out.printf("%10.2f\n", -17.7778);
		
		
		System.out.printf("%-12s|", "+20");
		
		System.out.printf("%10.2f\n", -6.6667);
		
		
		System.out.printf("%-12s|", "+30");
		
		System.out.printf("%10.2f\n", -1.1111);
	}
		
	
}